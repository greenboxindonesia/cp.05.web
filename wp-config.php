<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_SERVER["DB1_NAME"]);

/** MySQL database username */
define('DB_USER', $_SERVER["DB1_USER"]);

/** MySQL database password */
define('DB_PASSWORD', $_SERVER["DB1_PASS"]);

/** MySQL hostname */
define('DB_HOST', $_SERVER["DB1_HOST"].':'.$_SERVER["DB1_PORT"]);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4,c9wba2+_BGBJ8?UFMP<SzSL{|p7;QKsy^bt=|~-c56Iq77RW$Nbc@xd{_zfe$q');
define('SECURE_AUTH_KEY',  '`nEU_HXnI_;;4-|oeGD<}OM-~{x_f_!&uIw4wW=!d@9):#K&g,PjKGenu8pj&zS_');
define('LOGGED_IN_KEY',    '.i ^Wc5/GjfoW!C;41iBnotLkA4gW3GN7/C5CYBg|js+6Qp`4RV{WY6+:Pw2MB|L');
define('NONCE_KEY',        'yXSiUv69^+!}&d<6-+w_&y=Z}w-x3DLxtdyu6!oAX>qWvdQo,El#e-hz&{yDq`V_');
define('AUTH_SALT',        'Ad]W<6M[6X-O-^N@Xs4G)w2|[thG(Ft<agUP@xb<qgW>9F4m @9NuQ?HRG[TCi04');
define('SECURE_AUTH_SALT', 'fs`||W2Y8DT|V@,s>al{HcSUucM|&|+%./)N~)W3#+P|S8n!5cr)h{2m)I>I.pw=');
define('LOGGED_IN_SALT',   '_I4;1sVTA!9X@W4#HFF5(xM~-[oI#/&}_Vx}:k><GT1?iwd9*sx,~@<M~O#,iKAT');
define('NONCE_SALT',       'z~OL%<>$7+{W=~NR;m/R(*%J/AV5M#imEzQt1AL/(Mc+}C=gpV]BsaA-?1ivKpBX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cp_05_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
